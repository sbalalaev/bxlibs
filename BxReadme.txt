======================================
========== Library iByteriX ==========
======================================
ByteriX, 2013-2016. All right reserved.

Version 1.0.29 (19.09.2016)
- support of iOS 10 and fix background of the navigation panel

Version 1.0.28 (21.05.2016)
- BxDownloadStream & BxDownloadStreamWithResume : fix major security bug from stopping load data

Version 1.0.27 (09.05.2016)
- BxOldInputTableController crush fixed from nullable currentFieldName in didChangedValue

Version 1.0.26 (05.05.2016)
- BxOldInputTableController disable cell not hiddent if you want

Version 1.0.25 (29.04.2016)
- BxOldInputTableController disable set on the keyboard from variant field

Version 1.0.24 (10.04.2016)
- add refresh to BxOldInputTableController

Version 1.0.23 (07.04.2016)
- opened protected properties from BxOldInputTableController

Version 1.0.21 (04.04.2016)
- add the mock and sevice logger information

Version 1.0.20 (04.04.2016)
- add the mock for loading data with web services

Version 1.0.0 (16.03.2016)
- transfer this library to open source code
